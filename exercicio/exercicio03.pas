Program exercicio03 ;
type
TPlayer = record
	np,m,e,ef,a:integer;
	end;  
var
 
	j1,j2,j3:TPlayer;
	pont1,pont2,pont3:real;
Begin

	j1.np:= random(100);
	j1.m:= random(100);
	j1.e:= random(100);
	j1.ef:= random(100);
	j1.a:= random(100);
	
	pont1:= (((j1.e+(j1.a*0.7)) - j1.m)/(j1.np*0.2))+j1.ef;

	if (pont1<25)then
	begin 
	   write('jogador 1: Prata');
	end;
	
	if (pont1>25) and (pont1<50)then
	begin 
	   write('jogador 1: Ouro');
	end;
	
	if (pont1>50) and (pont1<70)then
	begin 
	   write('jogador 1: Platina');
	end;
	
	if (pont1>70) and (pont1<90)then
	begin 
	   write('jogador 1: Diamante');
	end;
	 
	 if (pont1>90)then
	begin 
	   write('jogador 1: Elite');
	end;
	j2.np:= random(100);
	j2.m:= random(100);
	j2.e:= random(100);
	j2.ef:= random(100);
	j2.a:= random(100);
	
	pont2:= (((j2.e+(j2.a*0.7)) - j2.m)/(j2.np*0.2))+j2.ef;

	if (pont2<25)then
	begin 
	   write('jogador 2: Prata');
	end;
	
	if (pont2>25) and (pont2<50)then
	begin 
	   write('jogador 2: Ouro');
	end;
	
	if (pont2>50) and (pont2<70)then
	begin 
	   write('jogador 2: Platina');
	end;
	
	if (pont2>70) and (pont2<90)then
	begin 
	   write('jogador 2: Diamante');
	end;
	 
	 if (pont2>90)then
	begin 
	   write('jogador 2: Elite');
	end;
	j3.np:= random(100);
	j3.m:= random(100);
	j3.e:= random(100);
	j3.ef:= random(100);
	j3.a:= random(100);
	
	pont3:= (((j3.e+(j3.a*0.7)) - j3.m)/(j3.np*0.2))+j3.ef;

	if (pont3<25)then
	begin 
	   write('jogador 3: Prata');
	end;
	
	if (pont3>25) and (pont3<50)then
	begin 
	   write('jogador 3: Ouro');
	end;
	
	if (pont3>50) and (pont3<70)then
	begin 
	   write('jogador 3: Platina');
	end;
	
	if (pont3>70) and (pont3<90)then
	begin 
	   write('jogador 3: Diamante');
	end;
	 
	 if (pont3>90)then
	begin 
	   write('jogador 3: Elite');
	end;
End.
